from pathlib import Path
import numpy as np
import pandas as pd
import datetime
# import re

# gen url from time


def gen_url(time_list,name):

    url_list = []

    for shift in time_list:
        time_shift = datetime.timedelta(hours=-2)
        shift[0]+=time_shift
        shift[1]+=time_shift
        # agenda 
        # agenda = 'balieutwente%40gmail.com'
        # agenda = 'maxkivits42%40gmail.com'
        agenda = 'ik0egsd360q4a9iedp34rsvc94%40group.calendar.google.com'

        #extract start/endtime
        # start = shift[0].strftime('%Y%m%dT%H%%M00')+'/'+shift[1].strftime('%Y%m%dT%H%%M00')
        start = shift[0].strftime('%Y%m%dT%H%M00')
        end = shift[1].strftime('%Y%m%dT%H%M00')

        #timezone
        timezone = 'Europe/Amsterdam'
        #location
        location = 'De%20Veldmaat%205,%207522%20NM%20Enschede'
        

        url = 'https://calendar.google.com/calendar/r/eventedit?'+'&src={}'.format(agenda)+'&text={}'.format(name)+'&dates={}Z/{}Z'.format(start, end)+'&ctz={}'.format(timezone)+'&location={}'.format(location)

        url_list.append(url)

    return url_list


# main func
def solve(csv_location, name='Max'):

    # 0 indexed colum containing dateframe
    date_column = 1
    # 0 indexed colum containing UB openingtime
    open_column = 2

    path = Path(csv_location)
    raw_data = pd.read_excel(path, engine='openpyxl')

    # print(raw_data)

    # Capitalize name
    capi_name = name.capitalize()

    # create time list, every entry represents a shift and is a tuple containing that shifts start & end date
    time_list = []
    for row in range(len(raw_data.index)):
        for col in range(raw_data.shape[1]):
            if raw_data.iloc[row, col] == capi_name:

                # find year day month
                timestr = (str(raw_data.iloc[row, date_column]).split()[0]+'-')

                # calc ub opentime midpoint
                times = raw_data.iloc[row, open_column].replace(
                    ' ', '').split('-')
                d1 = datetime.datetime.strptime(times[0], '%H.%M')
                d2 = datetime.datetime.strptime(times[1], '%H.%M')
                dt = (d2-d1)/2

                # create base time object
                base_start = datetime.datetime.strptime(
                    timestr+times[0], '%Y-%m-%d-%H.%M')
                base_end = datetime.datetime.strptime(
                    timestr+times[0], '%Y-%m-%d-%H.%M')+dt

                # add correct shift time
                if col == 4:  # TODO remove hardcode
                    base_start += dt
                    base_end += dt
                

                time_list.append([base_start, base_end])


    print('found {} entries for {}'.format(len(time_list), capi_name) +
          (' :(' if time_list == [] else '')+', Generating URLs...')

    # ~~~~~~~~~~~~~~~~~~~~~~~~

    url_list = gen_url(time_list,capi_name)

    for i in range(len(url_list)):
        print(url_list[i])

solve(
    "rooster _juni 2021.xlsx",
    'max')

# solve(
#     "/home/max/projects/ub_rooster_parser/test/test_file.xlsx",
#     'max')
